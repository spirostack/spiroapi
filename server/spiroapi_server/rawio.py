from __future__ import print_function, unicode_literals, division, absolute_import

import sys
import os
import io
import tornado.iostream


def make_stdio_raw():
    """
    Claim stdin/stdout as raw binary streams, and get them out of the way of
    anything that might try to use them as text streams. Also snags stderr.

    Redirects stdout and stderr to an internal pipe, and sets stdin to /dev/null.

    Returns new tornado PipeIOStream for input, output, and error.
    """
    # Dissassemble all the connections, so that accidental references don't cause problems
    if hasattr(sys.stdin, 'detach'):
        # Detach text io, returning buffer; detach buffer, returning raw
        sys.stdin.detach().detach()
        sys.stdout.detach().detach()

    fderrout, fderrin = os.pipe()


    # Mock in substitutes, so things don't break.
    sys.stdin = open(os.devnull, 'wt')
    sys.stdout = sys.stderr = os.fdopen(
        fderrin, 'w'
    )

    # Do the same thing on a fd level
    # Get the old stdin/out out of the way
    fdin = os.dup(0)
    fdout = os.dup(1)
    # We don't care about the old stderr; OpenSSH sends it to /dev/null

    # Get the new stdio in place
    os.dup2(sys.stdin.fileno(), 0)
    os.dup2(sys.stdout.fileno(), 1)
    os.dup2(sys.stderr.fileno(), 2)

    # Take fdin/out (the true stdin/out) and make tornado objects out of them.
    input_stream = tornado.iostream.PipeIOStream(fdin)
    output_stream = tornado.iostream.PipeIOStream(fdout)
    error_stream = tornado.iostream.PipeIOStream(fderrout)

    return input_stream, output_stream, error_stream
