import asyncio

class PendingRequest:
    def __init__(self, coro_factory):
        self._coro = coro_factory

    async def __aenter__(self):
        req = OngoingRequest()
        # PY37: asyncio.create_task()
        self._task = asyncio.ensure_future(self._coro(req))
        await req._started.wait()
        return req

    async def __aexit__(self, exc_type, exc, tb):
        self._task.cancel()

    async def __aiter__(self):
        async with self as req:
            async for values in req:
                yield values


class OngoingRequest:
    def __init__(self):
        self._queue = asyncio.Queue()
        self._started = asyncio.Event()

    async def _job_started(self, jid, speculated_minions):
        self.jid = jid
        self.speculated_minions = speculated_minions
        self._started.set()

    async def _got_result(self, mid, retcode, data):
        await self._queue.put((mid, retcode, data))

    async def _got_error(self, mid, error):
        await self._queue.put((mid, type(error), error))

    async def __aiter__(self):
        while True:
            mid, retcode, data = await self._queue.get()
            yield mid, retcode, data
